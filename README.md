
This project aims to provide a port of Slick2D to WebGL using Kotlin.
Since Kotlin compiles to JavaScript, the library can be used with any 
Web language.

Port of Slick2D
http://slick.ninjacave.com/

WebGL
https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL
